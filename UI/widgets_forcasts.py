import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings
import statsmodels.api as sm
from sklearn.model_selection import TimeSeriesSplit
import Forecasts.SksRoutine as ClassSksRoutine

from ipywidgets import Button, HBox, VBox, Checkbox, interactive, interact

import ipywidgets as widgets

class NaiveForecasts:
    """Contains naive methods for forecast timeseries."""
    def __init__(self, train, test, poll_name, event, plot):
        self._train_data = train
        self._test_data = test
        self._poll_name = poll_name
        self._event = event
        self._output = widgets.Output()
        self._pplot = plot
        self._sksr = ClassSksRoutine.SksRoutine()
        self._model_data = []
        self._plots = []
        self._seas_per = widgets.IntSlider(layout = {'width': '500px'}, 
            value=1, min=1, 
            max=365, step=1, 
            continuous_update=False, 
            description='days')
        self._forecast_days = widgets.IntSlider(layout = {'width': '500px'}, 
            value=7, min=1, 
            max=365, step=1, 
            continuous_update=False, 
            description='forcast days')
        self._splitt_slider = widgets.IntSlider(layout = {'width': '500px'}, 
            value=2, min=2, 
            max=10, step=1,
            continuous_update=False, 
            description='splitting')
        
        self._smoothing_slider = widgets.FloatSlider(
            layout = {'width': '500px'}, 
            value=0.95, min=0, max=1.0, step=0.01, 
            continuous_update=False, description='smoothing level')        
        self._smoothing_slope_slider = widgets.FloatSlider(
            layout = {'width': '500px'}, 
            value=0.2, min=0, max=1.0, step=0.01, 
            continuous_update=False, description='smoothing slope')
        self._draw_smoothing_slope_level_slider = widgets.FloatSlider(
            layout = {'width': '500px'}, 
            value=0.9, min=0, max=1.0, step=0.01, 
            continuous_update=False, description='damping slope')
        self._radio_btn_seasonal = widgets.RadioButtons(
            style={'description_width': 'initial'},
            options=[None,'add','mul'],
            value='mul',
            description='Seasonal param:',
            disabled=False,)
        self._radio_btn_trend = widgets.RadioButtons(
            style={'description_width': 'initial'},
            options=[None,'add','mul'],
            #value=None,
            description='Trend param:',
            disabled=False,)
        self._rdn_removebias = Checkbox(description='remove bias',value=True)
        self._rdn_boxcox = Checkbox(description='boxcox',value=False)
    @property
    def forcast_days(self):
        return self._forecast_days.value
    @property
    def subset_count(self):
        val_splitt = 2
        if not isinstance(self._splitt_slider, widgets.IntSlider):
            val_splitt = self._splitt_slider.new
        else:
            self._splitt_slider.value = val_splitt
        return val_splitt
    def _plot(self, forcast_data, b):
        my_plot = self._pplot.train_test_forecast(self._train_data, self._test_data, forcast_data)
    def _nested_cross_validation(self, b, routine, **kwargs):
        self._output.clear_output()
        self._model_data = []
        self._plots = []
        rmse_mean = 0
        with self._output:
            tss = TimeSeriesSplit(max_train_size=None, n_splits=self.subset_count)

            idx = 0
            for train_index, test_index in tss.split(self._train_data):
                curr_model_data = {}
                idx += 1
                X_train, X_test = self._train_data[train_index], self._train_data[test_index]
                curr_model_data["name"] = b.description
                
                y_hat = routine(X_train, X_test, **kwargs)
                    
                curr_model_data["rmse"] = self._sksr.metric_rmse(X_test, y_hat)
                rmse_mean += curr_model_data["rmse"]
                curr_model_data["mape"] = self._sksr.metric_mean_absolute_percentage_error(X_test, y_hat)
                curr_model_data["me"] = self._sksr.metric_mean_error(X_test, y_hat)
                curr_model_data["mae"] = self._sksr.metric_mean_absolute_error(X_test, y_hat)
                curr_model_data["mpe"] = self._sksr.metric_mean_percentage_error(X_test, y_hat)
                self._plots.append(y_hat)    
                self._model_data.append(curr_model_data)
                
            rmse_mean = rmse_mean / idx    
            df = pd.DataFrame(self._model_data)
            
            print(df)
            print('Mean RMSE of {} subsets: {:5.3f}'.format(idx, rmse_mean))
    def _plot_moving_avg(self, b):
        self._output.clear_output()
        with self._output:
            val = 7
            if not isinstance(self._seas_per, widgets.IntSlider):
                val = self._seas_per.new
            else:
                self._seas_per.value = val
            self._nested_cross_validation(b, self._sksr.moving_avg, mean_value=val)
            self._plot(self._sksr.moving_avg(self._train_data, self._test_data, mean_value=val), b)
    def _plot_naive(self, b):
        self._output.clear_output()
        self._nested_cross_validation(b, self._sksr.naive)
        with self._output:
            self._plot(self._sksr.naive(self._train_data, self._test_data), b)
    def _plot_simple_avg(self, b):
        self._output.clear_output()
        self._nested_cross_validation(b, self._sksr.simple_avg)
        with self._output:
            self._plot(self._sksr.simple_avg(self._train_data, self._test_data), b)
    def _plot_sgl_exp_smooth(self, b):
        self._output.clear_output()
        self._nested_cross_validation(b, self._sksr.sgl_exp_smooth, smoothing_level=self._smoothing_slider.value, optimized=False)
        with self._output:
            self._draw_smoothing()
            self._plot(self._sksr.sgl_exp_smooth(
                self._train_data, 
                self._test_data, 
                smoothing_level=self._smoothing_slider.value, 
                optimized=False), b)
    def _plot_holt_linear_trend(self, b):
        self._output.clear_output()
        self._nested_cross_validation(b, self._sksr.holt_linear_trend, 
                                      smoothing_level=self._smoothing_slider.value, 
                                      smoothing_slope=self._smoothing_slope_slider.value, 
                                      damping_slope=self._draw_smoothing_slope_level_slider.value, 
                                      optimized=False)
        with self._output:
            self._draw_smoothing_slope_level()
            self._plot(self._sksr.holt_linear_trend(self._train_data, self._test_data, 
                                                    smoothing_level=self._smoothing_slider.value, 
                                                    smoothing_slope=self._smoothing_slope_slider.value, 
                                                    damping_slope=self._draw_smoothing_slope_level_slider.value, 
                                                    optimized=False), b)
    def _plot_holt_winter_seasonal(self, b):
        warnings.filterwarnings("ignore")
        self._output.clear_output()

        with self._output:
            val = 365
            if not isinstance(self._seas_per, widgets.IntSlider):
                if self._seas_per.new <= 7:
                    self._seas_per.new = 365
                val = self._seas_per.new
            else:
                self._seas_per.value = val
            self._seas_per.value = val
            self._nested_cross_validation(b, self._sksr.holt_winter_seasonal, 
                                        seasonal_periods=self._seas_per.value, 
                                        trend=self._radio_btn_trend.value, 
                                        seasonal=self._radio_btn_seasonal.value,
                                        box_cox=self._rdn_boxcox.value,
                                        remove_bias=self._rdn_removebias.value)
            self._draw_trend_seasonal()
            self._plot(self._sksr.holt_winter_seasonal(self._train_data, self._test_data, 
                                                    seasonal_periods=self._seas_per.value, 
                                                    trend=self._radio_btn_trend.value, 
                                                    seasonal=self._radio_btn_seasonal.value,
                                                    box_cox=self._rdn_boxcox.value,
                                                    remove_bias=self._rdn_removebias.value), b)    
        warnings.filterwarnings("warn")
    def _seasonal_period(self, seasonal_slider):
        self._seas_per = seasonal_slider
    def _valid_subset(self, valid_subset_slider):
        self._valid_subset_slider = valid_subset_slider
    def _splitts(self, splitt_slider):
        self._splitt_slider = splitt_slider
    def smoothing_level(self, value):
        self._smoothing_level = value

    def _draw_smoothing(self):
        ui = widgets.HBox([self._smoothing_slider])
        out=widgets.interactive_output(self.smoothing_level, {'value':self._smoothing_slider})
        display(ui, out)
    def _draw_trend_seasonal(self):
        upperbox = widgets.HBox([
            self._radio_btn_seasonal,
            self._radio_btn_trend])
        lowerbox = widgets.HBox([
            self._rdn_removebias,
            self._rdn_boxcox])
        ui = VBox([upperbox, lowerbox])
        display(ui)
    def _draw_smoothing_slope_level(self):
        ui = widgets.VBox([
            self._smoothing_slider, 
            self._smoothing_slope_slider, 
            self._draw_smoothing_slope_level_slider])
        display(ui)
    def draw_buttons(self):
        methods = ['naive', 'simple avg', 
                   'moving avg', 'simple exp smoothing', 
                   'Holt linear trend', 'Holt Winter seasonal',
                   'SARIMAX']
        items = [Button(description=method) for method in methods]
        left_box = VBox([items[0], items[1]])
        middle_box = VBox([items[2], items[3]])
        right_box = VBox([items[4], items[5]])        
        h_box=HBox([left_box, middle_box, right_box])
        
        lab_t = widgets.Label('Seasonal period /Rolling mean:')
        lab_btn = widgets.Label('Please select model to analyze:')
        #lab_valid = widgets.Label('Please select validation count for subset:')
        lab_splitts = widgets.Label('How many parts/subsets should be divided into?')
        lab_forcast_days = widgets.Label('Forecast days:')
        v_box=VBox([lab_forcast_days, self._forecast_days,
                    lab_t, self._seas_per, 
                    lab_splitts, self._splitt_slider, 
                    lab_btn, h_box])

        display(v_box,self._output)
        self._seas_per.observe(self._seasonal_period, names='value')
        #self._valid_subset_slider.observe(self._valid_subset, names='value')
        self._splitt_slider.observe(self._splitts, names='value')
        items[0].on_click(self._plot_naive)
        items[1].on_click(self._plot_simple_avg)
        items[2].on_click(self._plot_moving_avg)
        items[3].on_click(self._plot_sgl_exp_smooth)
        items[4].on_click(self._plot_holt_linear_trend)
        items[5].on_click(self._plot_holt_winter_seasonal)

