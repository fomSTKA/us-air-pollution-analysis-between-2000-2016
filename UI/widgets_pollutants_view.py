from ipywidgets import HBox, interact, VBox
from numpy import array
from geopy.geocoders import Nominatim
from IPython.display import GeoJSON
from matplotlib.dates import DateFormatter
from statsmodels.tsa.arima_model import ARIMA

import UI.pollutants_plot as ClassPlot
import CrossCutting.analyze_missed as ClassAnalyzeMissed
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import ipywidgets as widgets
import numpy as np
import datetime

locator = Nominatim(user_agent='myGeocoder')

class Publisher:
    """Register ovserver classes."""
    def __init__(self, bag_of_polls):
        self.subscribers = dict()
        self.bag_of_polls = bag_of_polls
    def register(self, who, callback=None):
        if callback == None:
            callback = getattr(who, 'update')
        self.subscribers[who] = callback
    def unregister(self, who):
        del self.subscribers[who]
    def dispatch(self, pollutant, observe = None,):
        if observe == None:
            for subscriber, callback in self.subscribers.items():
                callback('unknown', pollutant, self.bag_of_polls)
        else:
            for subscriber, callback in self.subscribers.items():
                callback(observe, pollutant, self.bag_of_polls)

class ValueChecker:
    """Compilation of pollutant information depens on observe value."""
    def __init__(self, name):
        self.name = name
    def _output(self, elem, values, pollutant):
        values = values.drop_duplicates()
        unit = unit = values[[col for col in values.columns if 'Units' in col][0]].unique()[0]
        max_val = values[[col for col in values.columns if '1st Max Value' in col][0]].max()
        mean_val = values[[col for col in values.columns if 'Mean' in col][0]].mean()
        print('{:>3} Max Value: {:6.2f} {} Mean Value: {:6.2f} {}'.format(str(elem.pollutant), max_val, unit, mean_val, unit))
        
    def update_city(self, observe, pollutant, data):
        for elem in data:
            po_city = elem.pollutant.poll_filter[elem.pollutant.poll_filter.City.str.match(observe)]
            self._output(elem, po_city, pollutant)
    def update_state(self, observe, pollutant, data):
        for elem in data:
            po_state = elem.pollutant.poll_filter[elem.pollutant.poll_filter.State.str.match(observe)]
            self._output(elem, po_state, pollutant)
                
class GeoJs:
    """Representation of the geographical position."""
    def __init__(self, name):
        self.name = name
        self.data_js = None
    def update(self, observe, pollutant, data):
        for elem in data:
            if str(elem) == str(pollutant):
                self.data_js = elem.pollutant.poll_filter[elem.pollutant.poll_filter.City.str.match(observe)]
        df=self.data_js[['City', 'State']]
        self.data_js = df[df.City.str.match(observe)]
        location = locator.geocode(self.data_js.City.iloc[0] + ',' + self.data_js.State.iloc[0] + ', USA')
        self._geojs_output(location)
    def update_state(self, observe, pollutant, data):
        for elem in data:
            if str(elem) == str(pollutant):
                self.data_js = elem.pollutant.poll_filter[elem.pollutant.poll_filter.State.str.match(observe)]
        df=self.data_js[['City', 'State']]
        self.data_js = df[df.State.str.match(observe)]
        location = locator.geocode(self.data_js.City.iloc[0] + ',' + self.data_js.State.iloc[0] + ', USA')
        self._geojs_output(location)
    def _geojs_output(self, location):
        pic = GeoJSON(data={
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [location.longitude, location.latitude]
            }
        },
        layer_options={
            "minZoom" : 0,
            "maxZoom" : 4
        })
        display(pic)
        
class DataPlot:
    """Graphical display."""
    def __init__(self, name, event_col_name = 'City'):
        self._name = name
        self._event = event_col_name
    def plot_period(self, observe, pollutant, data):
        """Graphical plot for pollutant AQI."""

        selected_data = pd.DataFrame()
        for elem in data:
            if str(elem) == str(pollutant):
                if self._event == 'City':
                    selected_data = elem.pollutant.AQI_timeseries_by_city(observe)
                elif self._event == 'State':
                    selected_data = elem.pollutant.AQI_timeseries_by_state(observe)
                elif self._event == 'Country':
                    selected_data = elem.pollutant.AQI_timeseries_by_country(observe)
                    
        times=selected_data.index.to_series()
        
        col_name = str(pollutant) + ' AQI'
        unit = selected_data[str(pollutant) + ' Units'][0]
        
        plot = ClassPlot.PollutantsPlot(observe, self._event, True)
        plot.draw(times, selected_data, col_name, unit)
        
class DataRangeSlider:
    """Graphical display and Data-Range-Slider as setting option."""
    def __init__(self, name, event_col_name = 'City'):
        self._name = name
        self._event = event_col_name
        self._data = None
    def _create_period(self,time_range):
        """Graphical plot for pollutant AQI."""
        self._period = (time_range)
       
        col_name =[col for col in self._data.columns if 'AQI' in col][0]
        unit = self._data[[col for col in self._data.columns if 'Units' in col][0]].unique()[0]
        
        plot = ClassPlot.PollutantsPlot(self._name, self._event)
        df = self._data.copy()
        df = df.set_index('Date Local').resample('D')[col_name].mean()
        city_times = df.index.unique()
        plot.draw(city_times, df, col_name, unit)

    def receive_date(self, observe, pollutant, data):
        """Representation of time period Data-Range-Slider."""
        self._data = data
        for elem in data:
            if str(elem) == str(pollutant):
                if self._event == 'City':
                    self._data = elem.pollutant.poll_filter[elem.pollutant.poll_filter.City.str.match(observe)].dropna().drop_duplicates()
                if self._event == 'State':
                    self._data = elem.pollutant.poll_filter[elem.pollutant.poll_filter.State.str.match(observe)].dropna().drop_duplicates()
        self._timeplot()
    def _timeplot(self):
        city_times = self._data['Date Local'].unique()
        start_date = city_times.min()
        end_date = city_times.max()
        dates=pd.date_range(start_date, end_date, freq='D')
        options = [(date.strftime('%Y-%m-%d'), date) for date in dates]
        index = (0, len(options)-1)
        slider = widgets.SelectionRangeSlider(
            options=options,
            index=index,
            description='Time period',
            orientation='horizontal',
            continous_update=True,
            layout={'width': '800px'}
        )
        slider.observe(self._create_period, names='value')
        self.period=slider.value
        interact(self._create_period, time_range=slider)
        
class TestTrainSplitter:
    """Splitt time data into test and trainings data."""
    def __init__(self, name, event_col_name = 'City', adds = True):
        self._name = name
        self._event = event_col_name
        self._data = None
        self._data_fitted = None
        self._polname = None
        self._observe = None
        self._pollutant = None
        self._elem = None
        self._my_plot = ClassPlot.PollutantsPlot(self._polname, self._event, additionals = adds)
        self._splitt = None
    @property
    def pollutant(self):
        return self._pollutant
    @property
    def fitted_data(self):
        self._data_fitted
    @fitted_data.setter
    def fitted_data(self, data):
        self._data_fitted = data
    @property
    def event_col_name(self):
        return self._event
    @property
    def pol_name(self):
        return self._polname
    @property
    def original_data(self):
        return self._data
    @property
    def observed(self):
        return self._observe
    @property
    def test_data(self):
        return self._my_plot.test_data
    @property
    def train_data(self):
        return self._my_plot.train_data
    def _set_by_type(self):
        if self._event == 'City':
            self._data = self._elem.pollutant.AQI_timeseries_by_city(self._observe)
        elif self._event == 'State':
            self._data = self._elem.pollutant.AQI_timeseries_by_state(self._observe)
        elif self._event == 'Country':
            self._data = self._elem.pollutant.AQI_timeseries_by_country()
    def _plot_view(self, splitt, avg_val, std_val, tt_selection, fill_selection):
        self._my_plot.pol_name = self._polname
        df = self._data
        
        if self._event == 'City':
            df = self._elem.pollutant.AQI_timeseries(City=self._observe)
        elif self._event == 'State':
            df = self._elem.pollutant.AQI_timeseries(State=self._observe)
        elif self._event == 'Country':
            df = self._elem.pollutant.AQI_timeseries(Country=self._observe)
        
        cam = ClassAnalyzeMissed.ClsAnalyzeMissed()
        if fill_selection == 1:
            self._set_by_type()
        elif fill_selection == 2:
            self._data = cam.backward_fit(df, self._polname)
        elif fill_selection == 3:
            self._data = cam.forward_fit(df, self._polname)
        elif fill_selection == 4:
            self._data[self._polname] = cam.linear_fit(df, self._polname)['linear_fill'] 
        elif fill_selection == 5:
            self._data[self._polname] = cam.cubic_fit(df, self._polname)['cubic_fill']
        elif fill_selection == 6:
            self._data[self._polname] = cam.knn_mean_fit(df, self._polname)['knn_mean']
        elif fill_selection == 7:
            self._data[self._polname] = cam.seasonal_mean_fit(df, self._polname)['seasonal_mean']
            
        self._my_plot.draw_avg_mean(splitt,tt_selection, self._data, avg_val, std_val)
    def create_slider(self, observe, pollutant, data):
        """Representation of time period time range slider."""
        self._data = data
        self._observe = observe
        self._pollutant = pollutant
        self._polname = str(pollutant) + ' AQI'
        for elem in data:
            if str(elem) == str(pollutant):
                self._elem = elem
                self._set_by_type()
        count_max = len(self._data)
        lay = {'width': '500px'}
        slide_01 = widgets.IntSlider(layout = lay, min=0,
                                     max=count_max - 1, step=1, value=(count_max-100), description='splitt [idx]')
        slide_02 = widgets.IntSlider(layout = lay,min=2, max=355, step=1, value=30, description='avg [day]')
        slide_03 = widgets.IntSlider(layout = lay,min=2, max=355, step=1, value=30, description='std [day]')
        
        label_tt = widgets.Label('Training/Test:')
        tt_selection_04 = widgets.RadioButtons(
            options=[('Regular',1),('Rolling Mean',2),('Rolling Std.',3)],
            layout = {'width': '120px'},
            disabled=False,)
        label_fill = widgets.Label('Filled by:')
        fill_selection = widgets.RadioButtons(
            options=[('default',1),('backward',2),
                     ('forward',3),('linear',4),
                     ('cubic',5),('knn mean',6),
                     ('seasonal',7)],
            value = 4,
            disabled=False,)
        left_box = VBox([slide_01,slide_02,slide_03])
        tt_box = VBox([label_tt, tt_selection_04])
        fill_box = VBox([label_fill, fill_selection])
        right_box = HBox([tt_box, fill_box])
        ui = HBox([left_box,right_box])
        
        self._my_plot.additionals = True
        
        out=widgets.interactive_output(self._plot_view, 
                                       {'splitt':slide_01, 'avg_val':slide_02, 
                                        'std_val':slide_03, 'tt_selection':tt_selection_04,
                                       'fill_selection':fill_selection})
        display(ui,out)
class ArimaPlot:
    """Plot for ARIMA with SSR."""
    def __init__(self, data):
        self._data = data
        self._data.index = pd.DatetimeIndex(self._data.index.values,
                               freq=self._data.index.inferred_freq)
        self._data_diff = self._data - self._data.shift()
        self._results_MA = None
    @property
    def predict_ARIMA_difference(self):
        return self._results_MA
    @property
    def log_data_difference(self):
        return self._data_diff
    def TrendElemination(self):
        """difference between observation at a particular instant with that at the previous instant
           for higher stationarity with first order differencing from pandas (.shift)"""
        plt.figure(figsize=(20,10))
        plt.plot(self._data, label='log transformation')

        self._data_diff.dropna(inplace=True)
        plt.plot(self._data_diff, label='difference')
        plt.title('trend elemination')
        plt.legend()
        plt.show()
    def ARIMA(self, order=(0,0,0)):
        model = ARIMA(self._data, order)
        self._data_diff.dropna(inplace=True)

        self._results_MA = model.fit(disp=False)
        fig, ax = plt.subplots(figsize=(20, 10))

        ax.set_xlim(self._data.index[0], self._data.index[-1]) 
        
        myFmt = DateFormatter('%b %Y')
        ax.xaxis.set_tick_params(rotation=45)
        ax.xaxis.set_major_formatter(myFmt)
        
        ax.plot(self._data_diff, label='difference')
        ax.plot(self._results_MA.fittedvalues, color='red', label='fitted')
        plt.title('Sum of squared errors: %.4f'% sum((self._results_MA.fittedvalues-self._data_diff)**2))
        plt.legend()
        plt.show()
