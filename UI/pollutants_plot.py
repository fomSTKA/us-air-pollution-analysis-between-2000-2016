import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats

from matplotlib.dates import DateFormatter
from sklearn.metrics import mean_squared_error
from matplotlib.dates import DateFormatter
from math import sqrt
from matplotlib import rcParams
from datetime import datetime

font_parameters = {'legend.fontsize':'22',
                   'axes.titlesize' : '22',
                   'axes.labelsize' : '22',
                   'xtick.labelsize': '22',
                   'ytick.labelsize': '22'}

rcParams.update(font_parameters)

class PollutantsPlot:
    """Contains different plots for pollutant"""
    def __init__(self, poll_name, event, is_ref_index = False, additionals = True, *args):
        self._pol_name = poll_name
        self._event = event
        self._data = None
        self._train_data = None
        self._is_ref_index = is_ref_index
        self._additionals = additionals
        b = None
        for arg in args:
            b+=arg
        self._args = b
    @property
    def additionals(self):
        return self._additionals
    @additionals.setter
    def additionals(self, val):
        self._additionals = val
    @property
    def pol_name(self):
        return self._pol_name
    @pol_name.setter
    def pol_name(self, val):
        self._pol_name = val
    @property
    def test_data(self):
        return self._test_data
    @property
    def tmp_data(self):
        return self._data
    @property
    def train_data(self):
        my_data = None
        if self._tt_selection == 2:
            my_data = self._data[self._pol_name + ' mean']
        elif self._tt_selection == 3:
            my_data = self._data[self._pol_name + ' std']
        else:
            my_data = self._data[self._pol_name]
       
        return my_data[:self._splitt]
    def _ax_facecolor(self, ax):
        myFmt = DateFormatter('%b %Y')
        ax.xaxis.set_tick_params(rotation=45)
        ax.xaxis.set_major_formatter(myFmt)

        ax.axhspan(0, 50, facecolor='green', alpha=0.5)
        ax.axhspan(50, 100, facecolor='yellow', alpha=0.5)
        ax.axhspan(100, 150, facecolor='orange', alpha=0.5)
        ax.axhspan(150, 200, facecolor='red', alpha=0.5)
        ax.axhspan(200, 300, facecolor='purple', alpha=0.5)
        ax.axhspan(300, 500, facecolor='maroon', alpha=0.5)
    def draw_avg_mean(self, splitt, tt_selection, data, avg_val = 30, std_val = 30):
        self._avg_val = avg_val
        self._std_val = std_val
        self._splitt = splitt
        self._tt_selection = tt_selection

        _my_data = data.copy()
        _ncol = 2
        #fill by mean
        _my_data[self._pol_name] = _my_data[self._pol_name].fillna(_my_data[self._pol_name].rolling(self._avg_val).mean())
        self._train_data = _my_data[:self._splitt].copy()
        self._test_data = _my_data[(self._splitt + 1):len(_my_data)].copy()
        print(self._train_data.head())
        if self._additionals:
            _my_data[self._pol_name + ' mean'] = _my_data[self._pol_name].rolling(self._avg_val).mean()
            _my_data[self._pol_name + ' mean'] = _my_data[self._pol_name + ' mean'].fillna(method='bfill')
            _my_data[self._pol_name + ' mean'] = _my_data[self._pol_name + ' mean'].fillna(method='ffill')
            _my_data[self._pol_name + ' std'] = _my_data[self._pol_name].rolling(self._std_val).std()
            _my_data[self._pol_name + ' std'] = _my_data[self._pol_name + ' std'].fillna(method='bfill')
            _my_data[self._pol_name + ' std'] = _my_data[self._pol_name + ' std'].fillna(method='ffill')
        self._data = _my_data[:self._splitt].copy()
        fig, ax = plt.subplots(figsize=(20, 10))

        ax.set_xlim(data.index[0],data.index[-1])         
        ax.set_ylim(0,int(data[self._pol_name].max()*1.10))
        
        self._ax_facecolor(ax)
      
        plt.title('Train/Test Sets')

        ax.plot(self._train_data.index, self._train_data[self._pol_name], label='train')
        ax.plot(self._test_data.index, self._test_data[self._pol_name], color='C1', label='test')
        if self._additionals:
            _ncol = 4
            ax.plot(_my_data.index, _my_data[self._pol_name + ' mean'], color='red', label='rolling mean')
            ax.plot(_my_data.index, _my_data[self._pol_name + ' std'], color='white', label='rolling std')
        
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.02), ncol=_ncol, fancybox=True, shadow=True)
        plt.show()
        
        bplot=sns.boxplot( x=_my_data[self._pol_name] )
    def draw_raw(self, data, title, y_label):
        fig, ax = plt.subplots(figsize=(20, 10))
        ax.set_ylim(0,int(data.max()*1.10))
        times = data.index.unique()
        plt.title(title)
        plt.ylabel(y_label)
        self._ax_facecolor(ax)
        ax.set_xlim(times.min(),times.max())
        ax.plot(data)
        plt.show()
    def draw(self, x_values_ref, data, col_name, unit):
        nan = 0 #-10
        fig, ax = plt.subplots(figsize=(20, 10))
        
        if self._is_ref_index:
            ax.set_xlim(x_values_ref.iloc[0],x_values_ref.iloc[-1])
        else:
            ax.set_xlim(x_values_ref.min(),x_values_ref.max())
            
        ax.set_ylim(0 + nan,int(data.max()*1.10))
            
        self._ax_facecolor(ax)
        
        plt.title('Air Quality Index (AQI) Values')
        plt.ylabel('{} [{}]'.format(col_name,unit))
        ax.plot(x_values_ref,data)
        plt.show()
        bplot=sns.boxplot( x=data )
    def train_test_forecast(self, train, test, forcast_test):
        fig, ax = plt.subplots(figsize=(20, 10))

        ax.set_xlim(train.index[0],forcast_test.index[-1])
        y_max = int(train.max())
        if int(test.max()) > y_max:
            y_max = int(test.max())
        ax.set_ylim(0, y_max*1.10)

        self._ax_facecolor(ax)
        rsme_data = forcast_test
        if len(test) < len(forcast_test):
            rsme_data = forcast_test[:len(test)]

        rmse_test = self._RMSE(test, rsme_data)
        
        title='{} Train/Test Sets of {} (RMSE: {:8.4f})'.format(
            self._pol_name,
            self._event, 
            rmse_test)
        plt.title(title)

        ax.plot(train.index, train, label='train')
        ax.plot(test.index, test, color='C1', label='test')
        ax.plot(forcast_test.index, forcast_test, color='red', label=self._pol_name)

        plt.legend()
        plt.show()
    def _RMSE(self, test_data, forcast_data):
        if len(test_data) == len(forcast_data):
            return sqrt(mean_squared_error(test_data, forcast_data))
        
        return '---'

