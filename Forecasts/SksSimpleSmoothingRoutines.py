import numpy as np
import pandas as pd
import numbers
from numpy import array
from statsmodels.tsa.api import SimpleExpSmoothing
   
class SksSimpleSmoothingRoutines:
    def save_model_data(self, model_data):
        MyFile=open('./Forecasts/SksSimpleSmoothingRoutines.txt','w')
        for element in model_data:
            tofile=element.replace('[','')
            tofile=tofile.replace(']','')
            MyFile.write(tofile)
            MyFile.write('\n')
        MyFile.close()
    def _num(self, s):
        try:
            if s == 'True':
                return True
            elif s == 'False':
                return False
            elif s == 'None':
                return None
            elif s == "\'mul\'" or s == "\'add\'":
                return s.replace("'","")
            else:
                return float(s)
        except ValueError:
            return s
    def load_model_data(self):
        MyFile=open('./Forecasts/SksSimpleSmoothingRoutines.txt','r')
        model_data = []
        models = list()
        for line in MyFile:
            model_data.append(line.replace('\n',''))
        for elem in model_data:
            test = elem.split(',')
            cfg = list()
            for underelem in test:
                under = underelem.split(',')
                cfg.append(self._num(under[0].replace(' ','')))
            models.append(cfg)
        MyFile.close()
        return models
    def routine_forecast(self, history, config):
        """
        one-step Holt Winter’s Exponential Smoothing forecast
        """
        s = config[0]
        # define model
        history = array(history)
        model = SimpleExpSmoothing(history)
        # fit model
        model_fit = model.fit(optimized=True, smoothing_level=s)
        # make one step forecast
        yhat = model_fit.predict(len(history), len(history))
        return yhat[0]
    def routine_configs(self, seasonal=[None]):
        """create a set of exponential smoothing configs to try"""
        models = list()
        # define config lists
        start = 0
        stop = 1
        endpoint = 1
        s_params = np.linspace(start, stop, num = 21, endpoint = endpoint)
        # create config instances
        for s in s_params:
            cfg = [s]
            models.append(cfg)
        return models    
