from math import sqrt
from numpy import array
from multiprocessing import cpu_count
from joblib import Parallel, delayed, parallel_backend
from warnings import catch_warnings
from warnings import filterwarnings
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import TimeSeriesSplit
    
class SksBasicRoutines:
    def __init__(self):
        self._train = None
        self._test = None
        self._predictions = None
        self._func = None
    @property
    def prediction_data(self):
        return self._predictions
    def measure_rmse(self, actual, predicted):
        """root mean squared error or rmse"""
        return sqrt(mean_squared_error(actual, predicted))

    def train_test_split(self, data, n_test):
        """split a univariate dataset into train/test sets"""
        return data[:-n_test], data[-n_test:]

    def walk_forward_prediction(self, train, test, cfg, func):
        """walk-forward validation for univariate data"""
        self._predictions = list()
        # seed history with training dataset
        history = [x for x in train]
        # step over each time-step in the test set
        for i in range(len(test)):
            # fit model and make forecast for history
            yhat = func(history, cfg)
            # store forecast in list of predictions
            self._predictions.append(yhat)
            # add actual observation to history for the next loop
            history.append(test[i])
        # estimate prediction error
        error = self.measure_rmse(test, self._predictions)
        return error
    
    def walk_forward_validation(self, data, n_test, n_subsets, cfg, func):
        """walk-forward validation for univariate data"""
        predictions = list()
        # split dataset
        train, test = self.train_test_split(data, n_test)
        # seed history with training dataset
        history = [x for x in train]
        # step over each time-step in the test set
        for i in range(len(test)):
            # fit model and make forecast for history
            yhat = func(history, cfg)
            # store forecast in list of predictions
            predictions.append(yhat)
            # add actual observation to history for the next loop
            history.append(test[i])
        # estimate prediction error
        error = self.measure_rmse(test, predictions)
        return error

    def score_model_subset_walk(self, data, n_test, n_subsets, cfg, func, debug=False):
        """Subsets for cross validation and walk-forward."""
        result = None
        key = str(cfg)
        tss = TimeSeriesSplit(max_train_size=None, n_splits=n_subsets)
        error = 0
        cnt = 0
        rmse = 0
        for train_index, test_index in tss.split(data):
            last_idx = test_index[len(test_index)-1] + 1
            train = data[0:last_idx]
            try:
                with catch_warnings():
                    filterwarnings("ignore")
                    result = self.walk_forward_validation(train, n_test, n_subsets, cfg, func)
            except:
                error = None
                
            if result is not None:
                rmse += result
                cnt += 1
                #print(' > cnt %.3f result %.3f' % (cnt, result))
        if cnt >= 1:
            rmse = rmse / cnt
            print(' > Model[%s] %.3f' % (key, rmse))
        else:
            rmse = None

        return (key, rmse)
    
    def score_model_arima_and_co(self, data, n_test, n_subsets, cfg, func, debug=True):
        """score a model, return None on failure"""
        result = None
        # convert config to a key
        key = str(cfg)
        # show all warnings and fail on exception if debugging
        if debug:
            print('arima')
            result = func(data, cfg)
        else:
            # one failure during model validation suggests an unstable config
            try:
                # never show warnings when grid searching, too noisy
                with catch_warnings():
                    filterwarnings("ignore")
                    result = func(data, cfg)
            except:
                error = None
        # check for an interesting result
        if result is not None:
            print(' > Model[%s] %.3f' % (key, result.aic))
        return (key, result.aic)
    
    def score_model(self, data, n_test, n_subsets, cfg, func, debug=False):
        """score a model, return None on failure"""
        result = None
        # convert config to a key
        key = str(cfg)
        # show all warnings and fail on exception if debugging
        if debug:
            #walk = self.walk_forward_validation(...)
            #walk = self.train_test_subset_walk(...)
            result = self.walk_forward_validation(data, n_test, n_subsets, cfg, func)
        else:
            # one failure during model validation suggests an unstable config
            try:
                # never show warnings when grid searching, too noisy
                with catch_warnings():
                    filterwarnings("ignore")
                    result = self.walk_forward_validation(data, n_test, n_subsets, cfg, func)
            except:
                error = None
        # check for an interesting result
        if result is not None:
            print(' > Model[%s] %.3f' % (key, result))
        return (key, result)
    def grid_search(self, data, cfg_list, func, walk, n_test, n_subsets, parallel=True):
        """grid search configs"""
        self._func = func
        scores = None
        if parallel:
            # execute configs in parallel
#            executor = Parallel(n_jobs=cpu_count(), backend='multiprocessing')
#            executor = Parallel(n_jobs=cpu_count(), batch_size="auto", backend='threading')
            with parallel_backend("loky", inner_max_num_threads=2):
                executor = Parallel(n_jobs=cpu_count())
                tasks = (delayed(walk)(data, n_test, n_subsets, cfg, func) for cfg in cfg_list)
                scores = executor(tasks)
        else:
            scores = [walk(data, n_test, n_subsets, cfg, func) for cfg in cfg_list]
        # remove empty results
        scores = [r for r in scores if r[1] != None]
        # sort configs by error, asc
        scores.sort(key=lambda tup: tup[1])
        return scores
