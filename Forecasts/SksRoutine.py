import numpy as np
import pandas as pd
from math import sqrt
from sklearn.metrics import mean_squared_error
from statsmodels.tsa.api import ExponentialSmoothing, SimpleExpSmoothing, Holt

class SksRoutine:
    """Contains methods for calculations of naive forecast timeseries."""
    def naive(self, train, test, **kwargs):
        last_val_train = train[-1]
        y_hat = test.copy()
        y_hat.iloc[:] = last_val_train
        return y_hat
    def simple_avg(self, train, test, **kwargs):
        avg_val_train = train.mean()
        y_hat = test.copy()
        y_hat.iloc[:] = avg_val_train
        return y_hat
    def moving_avg(self, train, test, **kwargs):
        avg_val_train = train.rolling(window=kwargs['mean_value']).mean().iloc[-1]
        y_hat = test.copy()
        y_hat.iloc[:] = avg_val_train
        return y_hat
    def sgl_exp_smooth(self, train, test, **kwargs):
        train.index = pd.DatetimeIndex(train.index.values,
                            freq=train.index.inferred_freq)
        fit = SimpleExpSmoothing(train).fit(
            smoothing_level=kwargs['smoothing_level'],
            optimized=kwargs['optimized'])
        return fit.forecast(len(test))
    def holt_linear_trend(self, train, test, **kwargs):
        train.index = pd.DatetimeIndex(train.index.values,
                            freq=train.index.inferred_freq)
        fit = Holt(train).fit(
            smoothing_level=kwargs['smoothing_level'],
            smoothing_slope=kwargs['smoothing_slope'],
            damping_slope=kwargs['damping_slope'],
            optimized=kwargs['optimized'])
        return fit.forecast(len(test))
    def holt_winter_seasonal(self, train, test, **kwargs):
        train.index = pd.DatetimeIndex(train.index.values,
                               freq=train.index.inferred_freq)
#         trend = kwargs['trend']
#         if kwargs['trend'] == 'None':
#             trend = None
#         seasonal = kwargs['seasonal']
#         if kwargs['seasonal'] == 'None':
#             seasonal = None
            
        model = ExponentialSmoothing(train ,
            seasonal_periods=kwargs['seasonal_periods'] ,
            trend=kwargs['trend'], 
            seasonal=kwargs['seasonal'],
            damped=False)
        model_fit = model.fit(optimized=True,
                use_boxcox=kwargs['box_cox'],
                remove_bias=kwargs['remove_bias'])
        return model_fit.forecast(len(test))
    
    def metric_rmse(self, train_data, test_data):
        if len(train_data) == len(test_data):
            return sqrt(mean_squared_error(train_data, test_data))
        return '---'
    def metric_mean_absolute_percentage_error(self, train_data, test_data):
        if len(train_data) == len(test_data):
            return np.mean(np.abs(train_data - test_data)/np.abs(test_data))
        return '---'
    def metric_mean_error(self, train_data, test_data):
        if len(train_data) == len(test_data):
            return np.mean(train_data - test_data)
        return '---'
    def metric_mean_absolute_error(self, train_data, test_data):
        if len(train_data) == len(test_data):
            return np.mean(np.abs(train_data - test_data))
        return '---'
    def metric_mean_percentage_error(self, train_data, test_data):
        if len(train_data) == len(test_data):
            return np.mean(np.abs(train_data - test_data)/(test_data))
        return '---'
