import numpy as np
import pandas as pd
import itertools
import pmdarima as pm
from statsmodels.tsa.arima_model import ARIMA

class SksARIMA:
    """Contains methods for ARIMA forecast timeseries."""
    def __init__(self, poll_name, event):
        self._poll_name = poll_name
        self._event = event
        self._conf = None
    def save_model_data(self, model_data):
        if len(model_data) >= 1:
            MyFile=open('./Forecasts/SksARIMA.txt','w')
            for element in model_data:
                pass
                tofile=element.replace('[','')
                tofile=tofile.replace(']','')
                MyFile.write(tofile)
                MyFile.write('\n')
            MyFile.close()
    def _num(self, s):
        try:
            if s == 'True':
                return True
            elif s == 'False':
                return False
            elif s == 'None':
                return None
            else:
                return float(s)
        except ValueError:
            return s
    def load_model_data(self):
        MyFile=open('./Forecasts/SksARIMA.txt','r')
        model_data = []
        models = list()
        for line in MyFile:
            model_data.append(line.replace('\n',''))
        for elem in model_data:
            test = elem.split(',')
            cfg = list()
            for underelem in test:
                under = underelem.split(',')
                cfg.append(self._num(under[0]))
            models.append(cfg)
        MyFile.close()
        return models

    def auto(self, data):
        """
        Automatic ARIMA
        """
        model = pm.auto_arima(data, start_p=1, start_q=1,
                      test='adf',       # use adftest to find optimal 'd'
                      max_p=3, max_q=3, # maximum p and q
                      m=1,              # frequency of series
                      d=None,           # let model determine 'd'
                      seasonal=False,   # No Seasonality
                      start_P=0, 
                      D=0, 
                      trace=True,
                      error_action='ignore',  
                      suppress_warnings=True, 
                      stepwise=True)

        print(model.summary())
        return model
    def routine_configs(self, seasonal=[None]):
        """create a set of exponential smoothing configs to try"""
        models = list()
        # create config instances
        # PDQ, Seasonal - Values
        P_params = Q_params = D_params = range(0, 6)

        for p in P_params:
            for d in D_params:
                for q in Q_params:
                    cfg = [p,d,q]
                    models.append(cfg)
        return models
    
    def _difference(self, dataset, interval=1):
        diff = list()
        for i in range(interval, len(dataset)):
            value = dataset[i] - dataset[i - interval]
            diff.append(value)
        return np.array(diff)
    def _inverse_difference(self, history, yhat, interval=1):
        return yhat + history[-interval]
    def forecast(self, train, order=(1,1,1)):
        values = train.values
        days = 365
        differenced = self._difference(values, days)
        model = ARIMA(differenced, order=order)
        model_fit = model.fit(disp=False)
        forecast = model_fit.forecast()[0]
        forecast = self._inverse_difference(values, forecast, days)
        return forecast
    def forecast_step02(self, train, order=(1,1,1)):
        values = train.values
        days = 365
        differenced = self._difference(values, days)
        model = ARIMA(differenced, order=order)
        model_fit = model.fit(disp=False)
        start_index = len(differenced)
        end_index = len(differenced)
        forecast = model_fit.predict(start=start_index, end=end_index)
        forecast = self._inverse_difference(values, forecast, days)
        return forecast
    def forecasted_from_training(self, train, test, order=(1,1,1), future = 0):
        """Forecast with model from training data.
        Here is test data count used, for forecasting."""
        values = train.values
        days = 365
        differenced = self._difference(values, days)
        model = ARIMA(differenced, order=order)
        model_fit = model.fit(disp=False)

        fc, se, conf = model_fit.forecast(steps=len(test) + future, alpha=0.05)
        forecast = pd.Series(fc)
        history = [x for x in values]
        day = 1
        self._conf = conf
        for yhat in forecast:
            inverted = self._inverse_difference(history, yhat, days)
            #print('Day %d: %f' % (day, inverted))
            history.append(inverted)
            day += 1
        return history
    def forecasted_from_history(self, train, test, order=(1,1,1)):
        """Forecastin point by point inclucive last
        predicted point."""
        values = train.values
        history = [x for x in values]
        model = ARIMA(history, order=(1,1,1))
        model_fit = model.fit(disp=False)
        prediction = list()
        for t in test:
            model = ARIMA(history, order=order)
            model_fit = model.fit(disp=False)
            y_hat = model_fit.forecast()[0]
            prediction.append(y_hat)
            observed = t
            history.append(observed)
            #print('predicted=%f, expected=%f' % (y_hat, observed))
        return prediction
