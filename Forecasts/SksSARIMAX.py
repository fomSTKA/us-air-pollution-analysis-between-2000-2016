import numpy as np
import pandas as pd
import pmdarima as pm
from statsmodels.tsa.statespace.sarimax import SARIMAX

class SksSARIMAX:
    """Contains methods for SARIMAX forecast timeseries."""
    def __init__(self, poll_name, event):
        self._poll_name = poll_name
        self._event = event
        self._conf = None
    def save_model_data(self, model_data):
        if len(model_data) >= 1:
            MyFile=open('./Forecasts/SksSARIMAX.txt','w')
            for element in model_data:
                tofile=element.replace('[','')
                tofile=tofile.replace(']','')
                MyFile.write(tofile)
                MyFile.write('\n')
            MyFile.close()
    def _num(self, s):
        try:
            if s == 'True':
                return True
            elif s == 'False':
                return False
            elif s == 'None':
                return None
            elif "\'" in s:
                return s.replace("'","")
            else:
                return float(s)
        except ValueError:
            return s
    def load_model_data(self):
        MyFile=open('./Forecasts/SksSARIMAX.txt','r')
        model_data = []
        models = list()
        cfg = list()
        for line in MyFile:
            model_data.append(line.replace('\n',''))
        for elem in model_data:
            test = elem.split(',')
            cfg = list()
            pdq = ""
            for underelem in test:
                if "(" in underelem or ")" in underelem:
                    underelem = underelem.replace("(","")
                    underelem = underelem.replace(")","")
                    under = underelem.split(',')
                else:
                    under = underelem.split(',')
                cfg.append(self._num(under[0].replace(' ','')))
        models.append(cfg)
        MyFile.close()

        P,D,Q,p,d,q,s,n = models[2]
        PDQ = (int(P),int(D),int(Q))
        models = [PDQ,int(p),int(d),int(q),int(s),n]
        return models

    def _difference(self, dataset, interval=1):
        diff = list()
        for i in range(interval, len(dataset)):
            value = dataset[i] - dataset[i - interval]
            diff.append(value)
        return np.array(diff)
    def _inverse_difference(self, history, yhat, interval=1):
        return yhat + history[-interval]
    def forecasted_from_training(self, train, test, order=[(1,1,1),(1,0,1,12),'ct'], future = 0):
        """Forecast with model from training data.
        Here is test data count used, for forecasting.
        ‘n’ = no trend
        ’c’ = constant
        ’t’ = linear
        ’ct’= constant, linear trend
        """
        order, s_order, trend = order
        values = train.values
        days = 365
        differenced = self._difference(values, days)
#         print('order {} sorder {} trend {}'.format(order, s_order, trend))
        model = SARIMAX(differenced, order=order, seasonal_order=s_order, trend=trend, enforce_stationarity=False, enforce_invertibility=False)
        model_fit = model.fit(disp=False)
        fc = model_fit.forecast(steps=len(test) +  future, alpha=0.05)
        forecast = pd.Series(fc)
        history = [x for x in values]
        day = 1
        for yhat in forecast:
            inverted = self._inverse_difference(history, yhat, days)
            #print('Day %d: %f' % (day, inverted))
            history.append(inverted)
            day += 1
        return history
    def forecasted_from_history(self, train, test, order=(1,1,1)):
        """Forecastin point by point inclucive last
        predicted point."""
        values = train.values
        history = [x for x in values]
        model = ARIMA(history, order=(1,1,1))
        model_fit = model.fit(disp=False)
        prediction = list()
        for t in test:
            model = ARIMA(history, order=order)
            model_fit = model.fit(disp=False)
            y_hat = model_fit.forecast()[0]
            prediction.append(y_hat)
            observed = t
            history.append(observed)
            #print('predicted=%f, expected=%f' % (y_hat, observed))
        return prediction
    def auto(self, data):
        """
        Automatic SARIMA
        """
        smodel = pm.auto_arima(data, start_p=1, start_q=1,
                         test='adf',
                         max_p=3, max_q=3, m=12,
                         start_P=0, seasonal=True,
                         d=None, D=1, trace=True,
                         error_action='ignore',  
                         suppress_warnings=True, 
                         stepwise=True)

        print(smodel.summary())
        return smodel
    def routine_configs(self, seasonal=[None]):
        """create a set of exponential smoothing configs to try"""
        models = list()
        # create config instances
        # PDQ, Seasonal - Values
        P_params = Q_params = D_params = range(0, 3)
        PDQ_params = (2,0,3)
        s_params = seasonal
        n_params = ['n']
        for p in P_params:
            for d in D_params:
                for q in Q_params:
                    for s in s_params:
                        for n in n_params:
                            cfg = [PDQ_params,p,d,q,s,n]
                            models.append(cfg)
        return models
    def routine_forecast(self, data, config):
        """Forecast with model from training data.
        Here is test data count used, for forecasting.
        ‘n’ = no trend
        ’c’ = constant
        ’t’ = linear
        ’ct’= constant, linear trend
        """
        PDQ,p,d,q,s,n = config
        data.index = pd.DatetimeIndex(train.index.values,
                               freq='D')
        mod = SARIMAX(data,
                    order=PDQ,
                    seasonal_order=(p,d,q,s),
                    trend=n,
                    enforce_stationarity=False,
                    enforce_invertibility=False)
        results = mod.fit()
        return results



