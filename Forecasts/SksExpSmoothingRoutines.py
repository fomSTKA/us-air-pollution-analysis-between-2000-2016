import pandas as pd
from numpy import array
from statsmodels.tsa.api import ExponentialSmoothing

class SksExpSmoothingRoutines:
    def save_model_data(self, model_data):
        MyFile=open('./Forecasts/SksExpSmoothingRoutines.txt','w')
        for element in model_data:
            tofile=element.replace('[','')
            tofile=tofile.replace(']','')
            MyFile.write(tofile)
            MyFile.write('\n')
        MyFile.close()
    def _num(self, s):
        try:
            if s == 'True':
                return True
            elif s == 'False':
                return False
            elif s == 'None':
                return None
            elif s == "\'mul\'" or s == "\'add\'":
                return s.replace("'","")
            else:
                return float(s)
        except ValueError:
            return s
    def load_model_data(self):
        MyFile=open('./Forecasts/SksExpSmoothingRoutines.txt','r')
        models = list()
        model_data = []
        for line in MyFile:
            model_data.append(line.replace('\n',''))
        for elem in model_data:
            test = elem.split(',')
            cfg = list()
            for underelem in test:
                under = underelem.split(',')
                cfg.append(self._num(under[0].replace(' ','')))
            models.append(cfg)
        MyFile.close()
        return models
    def routine_forecast(self, history, config):
        """
        one-step Holt Winter’s Exponential Smoothing forecast
        """
        t,d,s,p,b,r = config
        # define model
        history = array(history)
        model = ExponentialSmoothing(history, trend=t, damped=d, seasonal=s, seasonal_periods=p)
        # fit model
        model_fit = model.fit(optimized=True, use_boxcox=b, remove_bias=r)
        # make one step forecast
        yhat = model_fit.predict(len(history), len(history))
        return yhat[0]
    def routine_configs(self, seasonal=[None]):
        """create a set of exponential smoothing configs to try"""
        models = list()
        # define config lists
        t_params = ['add', 'mul', None]
        d_params = [True, False]
        s_params = ['add', 'mul', None]
        p_params = seasonal
        b_params = [True, False]
        r_params = [True, False]
        # create config instances
        for t in t_params:
            for d in d_params:
                for s in s_params:
                    for p in p_params:
                        for b in b_params:
                            for r in r_params:
                                cfg = [t,d,s,p,b,r]
                                models.append(cfg)
        return models
