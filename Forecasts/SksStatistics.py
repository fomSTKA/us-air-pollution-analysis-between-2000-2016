import pandas as pd
import warnings
from statsmodels.tsa.stattools import adfuller, kpss

class SksTestStatistics:
    """Contains methods for statistics."""
    def _execute(func):
        def wrapper(data):
            warnings.filterwarnings("ignore")
            func(data)
            warnings.filterwarnings("default")
        return wrapper
    @_execute
    def test_stationarity(data):
        #Perform Dickey-Fuller test:
        print('##############################')
        print('Results of Dickey-Fuller Test:')
        dftest = adfuller(data, autolag='AIC')
        dfoutput = pd.Series(dftest[0:4], 
            index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
        for key,value in dftest[4].items():
            dfoutput['Critical Value (%s)'%key] = value
        print(dfoutput)
    @_execute
    def test_trend_stationarity(data):
        #KPSS Test
        print('##############################')
        print('Results of KPSS Test:')
        dkpss = kpss(data, regression='c', lags='auto')
        dfoutput = pd.Series(dkpss[0:2], 
            index=['Test Statistic','p-value'])
        for key,value in dkpss[3].items():
            dfoutput['Critical Value (%s)'%key] = value
        print(dfoutput)