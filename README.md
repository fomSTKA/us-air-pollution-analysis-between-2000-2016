#Analysis and prediction of US air pollution between 2000 - 2016

For requirements please refer to the requirements.txt. You can use that for configure your python environment.

Please start that project with uspollution.ipynb notebook.
That notebook runs from top to bottom.

Please note that you can select during execution counties, cities and air quality gas type. You can select the time periods for the training as well as for the prediction.

