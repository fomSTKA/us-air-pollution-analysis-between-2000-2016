import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer
import CrossCutting.analyze_missed as ClassAnalyzeMissed
import warnings

#Dependency Injection
#Listing of injection for pollutant modules.

class ClsPollutants(object):
    """Pollutant base class.
        Carbon monoxide CO,
        Ozone O3,
        Oxides of nitrogen NOx, 
        Nitrogen dioxide NO2,
        Sulfur dioxide SO2,
        ...
    """
    def __init__(self, data, regex):
        self._regex_data = data.filter(regex = regex)
        self._col_name = str(self) + ' AQI'
    @property
    def poll_filter(self):
        return self._regex_data
    @property
    def _poll_AQI_Date_Unit(self):
        return self._regex_data[[self._col_name, 'Date Local', 'City', 'State', str(self) + ' Units']]
        
    def get_data_year_city(self,year,city):
        """
        Get data with given year and city.
        """
        try:
            dfc = self._regex_data[self._regex_data['City'].str.contains(city)]
            start_date = '{}-01-01'.format(year)
            end_date = '{}-01-31'.format(year)
            return dfc[dfc['Date Local'].between(start_date, end_date)]
        except:
            return

    def AQI_timeseries(self, **kwargs):
        df = self._poll_AQI_Date_Unit.copy()

        for key in kwargs:
            if key == 'Country':
                df = self.AQI_timeseries_by_country()
            else:
                df = df[df[key].str.contains(kwargs[key])]
                df = self._adaptation(df)
        return df
        
    def AQI_timeseries_by_city(self, city = 'Roosevelt'):
        df_city = self._poll_AQI_Date_Unit.copy()
        df_city = df_city[df_city.City.str.contains(city)]
        #print('AQI by city')
        df_city = df_city.drop_duplicates().dropna()        
        df_city = self._remove_outlier(df_city)
        df_city = self._adaptation(df_city)
        return self._data_imputer(df_city)
    
    def AQI_timeseries_by_state(self, state = 'Wyoming'):
        df_state = self._poll_AQI_Date_Unit.copy()
        df_state = df_state[df_state.State.str.contains(state)]
        #print('AQI by state')
        del df_state['City']
        df_state = df_state.drop_duplicates().dropna()        
        df_state = self._remove_outlier(df_state)
        df_state = self._adaptation(df_state)
        return self._data_imputer(df_state)

    def AQI_timeseries_by_country(self):
        df_country = self._poll_AQI_Date_Unit
        df_country = df_country.drop_duplicates().dropna()
        df_country = self._remove_outlier(df_country)
        df_country = df_country.groupby('Date Local').mean()
        df_country.index = pd.to_datetime(df_country.index)
        return df_country

    def _adaptation(self, df):
        time_span = df['Date Local'].unique()
        start_date = time_span.min()
        end_date = time_span.max()

        new_date_range = pd.date_range(start_date, end_date, freq='D')
        
        df = df.set_index('Date Local')
        df.index = pd.DatetimeIndex(df.index)

        agg_types_dict = {
            self._col_name:'mean',
            'State':'last',
            str(self) + ' Units': 'last'}
        df = df.resample('D').agg(agg_types_dict)

        df = df.reindex(new_date_range, fill_value=None)

        return df
        
    def _data_imputer(self, df_in):
        imp = SimpleImputer(missing_values = np.nan, strategy = 'mean')
        df_in[self._col_name] = imp.fit_transform(df_in[[self._col_name]]).ravel()
        return df_in    
    
    def _remove_outlier(self, df_in):
        q1 = df_in[self._col_name].quantile(0.05)
        q3 = df_in[self._col_name].quantile(0.95)
        iqr = q3-q1
        fence_low  = q1-1.5*iqr
        fence_high = q3+1.5*iqr
        df_out = df_in.loc[(df_in[self._col_name] > fence_low) & (df_in[self._col_name] < fence_high)]
        return df_out
    
class ClsCarbonMonoxide(ClsPollutants):
    """CO, carbon monoxide"""
    def __init__(self, data):
        self.__regex = '^((?!O3|NOx|NO2|SO2).)*$'
        super().__init__(data, self.__regex)
    def __str__(self):
        return 'CO'
        
class ClsOzone(ClsPollutants):
    """O3, ozone"""
    def __init__(self, data):
        self.__regex = '^((?!CO|NOx|NO2|SO2).)*$'
        super().__init__(data, self.__regex)
    def __str__(self):
        return 'O3'
    
class ClsNitrogenDioxide(ClsPollutants):
    """NO2, nitrogen dioxide"""
    def __init__(self, data):
        self.__regex = '^((?!CO|NOx|O3|SO2).)*$'
        super().__init__(data, self.__regex)
    def __str__(self):
        return 'NO2'
    
class ClsSulfurDioxide(ClsPollutants):
    """SO2, sulfur dioxide"""
    def __init__(self, data):
        self.__regex = '^((?!CO|NOx|NO2|O3).)*$'
        super().__init__(data, self.__regex)
    def __str__(self):
        return 'SO2'
    
class ClsPollutant(object):
    """Concrete representation of pollutant."""
    def __init__(self, pollutant):
        """Initializer."""
        #Pollutant injection
        self._pollutant = pollutant

    def __repr__(self):
        return '{}'.format(self._pollutant)

    @property
    def pollutant(self):
        return self._pollutant
    
