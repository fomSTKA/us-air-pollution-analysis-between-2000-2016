from scipy.interpolate import interp1d
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import numpy as np
import warnings

class ClsAnalyzeMissed:
    def linear_fit(self, df, col_name):
        df['rownum'] = np.arange(df.shape[0])
        df_nona = df.dropna(subset = [col_name])
        f = interp1d(df_nona['rownum'], df_nona[col_name])
        df['linear_fill'] = f(df['rownum'])
        return df
    def backward_fit(self, df, col_name):
        df_bfill = df.bfill()
        return df_bfill
    def forward_fit(self, df, col_name):
        df_ffill = df.ffill()
        return df_ffill
    def cubic_fit(self, df, col_name):
        df['rownum'] = np.arange(df.shape[0])
        df_nona = df.dropna(subset = [col_name])
        f2 = interp1d(df_nona['rownum'], df_nona[col_name], kind='cubic')
        df['cubic_fill'] = f2(df['rownum'])
        return df
    def _knn_mean(self, ts, n):
        out = np.copy(ts)
        for i, val in enumerate(ts):
            if np.isnan(val):
                n_by_2 = np.ceil(n/2)
                lower = np.max([0, int(i-n_by_2)])
                upper = np.min([len(ts)+1, int(i+n_by_2)])
                ts_near = np.concatenate([ts[lower:i], ts[i:upper]])
                with warnings.catch_warnings():
                    warnings.filterwarnings('error')
                    try:
                        out[i] = np.nanmean(ts_near)
                    except RuntimeWarning:
                        out[i] = np.NaN
        return out
    def knn_mean_fit(self, df, col_name):
        df['knn_mean'] = self._knn_mean(df[col_name].values, 8)
        return df
    def _seasonal_mean(self, ts, n, lr=0.7):
        """
        Compute the mean of corresponding seasonal periods
        ts: 1D array-like of the time series
        n: Seasonal window length of the time series
        """
        warnings.simplefilter("ignore")
        out = np.copy(ts)
        for i, val in enumerate(ts):
            if np.isnan(val):
                ts_seas = ts[i-1::-n]  # previous seasons only
                if np.isnan(np.nanmean(ts_seas)):
                    ts_seas = np.concatenate([ts[i-1::-n], ts[i::n]])  # previous and forward
                out[i] = np.nanmean(ts_seas) * lr
        warnings.simplefilter("default")
        return out
    def seasonal_mean_fit(self, df, col_name):
        df['seasonal_mean'] = self._seasonal_mean(df[col_name], n=12, lr=1.25)
        return df
    def analyze(self, df_original, df_missed):

        fig, axes = plt.subplots(7, 1, sharex=True, figsize=(30, 20))
        plt.rcParams.update({'xtick.bottom' : False})

        ## 1. Actual -------------------------------
        df_original.plot(title='Actual', ax=axes[0], label='Actual', color='red', style=".-")
        df_missed.plot(title='Actual', ax=axes[0], label='Actual', color='green', style=".-")
        axes[0].legend(["Missing Data", "Available Data"])

        ## 2. Forward Fill --------------------------
        df_ffill = df_missed.ffill()
        error = np.round(mean_squared_error(df_original['value'], df_ffill['value']), 2)
        df_ffill['value'].plot(title='Forward Fill (MSE: ' + str(error) +")", ax=axes[1], label='Forward Fill', style=".-")

        ## 3. Backward Fill -------------------------
        df_bfill = df_missed.bfill()
        error = np.round(mean_squared_error(df_original['value'], df_bfill['value']), 2)
        df_bfill['value'].plot(title="Backward Fill (MSE: " + str(error) +")", ax=axes[2], label='Back Fill', color='firebrick', style=".-")

        ## 4. Linear Interpolation ------------------
        df_missed = self.linear_fit(df_missed, 'value')
        error = np.round(mean_squared_error(df_original['value'], df_missed['linear_fill']), 2)
        df_missed['linear_fill'].plot(title="Linear Fill (MSE: " + str(error) +")", ax=axes[3], label='Cubic Fill', color='brown', style=".-")

        ## 5. Cubic Interpolation --------------------
        df_missed = self.cubic_fit(df_missed, 'value')
        error = np.round(mean_squared_error(df_original['value'], df_missed['cubic_fill']), 2)
        df_missed['cubic_fill'].plot(title="Cubic Fill (MSE: " + str(error) +")", ax=axes[4], label='Cubic Fill', color='red', style=".-")

        # # Interpolation References:
        # # https://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html
        # # https://docs.scipy.org/doc/scipy/reference/interpolate.html

        ## 6. Mean of 'n' Nearest Past Neighbors ------
        df_missed = self.knn_mean_fit(df_missed, 'value')
        error = np.round(mean_squared_error(df_original['value'], df_missed['knn_mean']), 2)
        df_missed['knn_mean'].plot(title="KNN Mean (MSE: " + str(error) +")", ax=axes[5], label='KNN Mean', color='tomato', alpha=0.5, style=".-")

        ## 7. Seasonal Mean ----------------------------
        df_missed = self.seasonal_mean_fit(df_missed, 'value')
        error = np.round(mean_squared_error(df_original['value'], df_missed['seasonal_mean']), 2)
        df_missed['seasonal_mean'].plot(title="Seasonal Mean (MSE: " + str(error) +")", ax=axes[6], label='Seasonal Mean', color='blue', alpha=0.5, style=".-")