import UI.pollutants_plot as ClassPlot
import Forecasts.SksARIMA as ClassArima
import Forecasts.SksSARIMAX as ClassSarimax
import pandas as pd
import ipywidgets as widgets
import inspect
import dateutil.relativedelta as datedelta

from ipywidgets import Button, HBox, VBox

class Builder:
    def __init__(self, data, poll_name, event, splitt_pos = 5000):
        self._data = data
        self._poll_name = poll_name
        self._event = event
        self._splitt_pos = splitt_pos
        self._arima = ClassArima.SksARIMA(poll_name, event)
        self._sarimax = ClassSarimax.SksSARIMAX(poll_name, event)
        self._bu_plot = ClassPlot.PollutantsPlot(poll_name, event)
        self._ui_frame = None
        self._train = self._data[:self._splitt_pos].iloc[:,0]
        self._test = self._data[self._splitt_pos + 1:].iloc[:,0]
        self._output = widgets.Output()
    def train_test_forecast_sarimax(self, order=[(1,1,1),(1,0,1,12),'ct'], future = 0):
        train_and_test_forcast = self._sarimax.forecasted_from_training(self._train, self._test, order, future)
        test_forcast = train_and_test_forcast[len(self._train):]
        dr_time = pd.date_range(start=self._test.index[0], periods=len(self._test) + future, freq='D')
        df_forecasts = pd.DataFrame(test_forcast, index=dr_time)
        self._bu_plot.train_test_forecast(self._train, self._test, df_forecasts)
        text = self._poll_name + ' prediction'
        df_forecasts.columns = [text]
        print(df_forecasts.tail(20))
    def train_test_forecast_arima(self, order=(1,1,1), future = 0):
        train_and_test_forcast = self._arima.forecasted_from_training(self._train, self._test, order, future)
        test_forcast = train_and_test_forcast[len(self._train):]
        dr_time = pd.date_range(start=self._test.index[0], periods=len(self._test) + future, freq='D')
        df_forecasts = pd.DataFrame(test_forcast, index=dr_time)
        self._bu_plot.train_test_forecast(self._train, self._test, df_forecasts)
        text = self._poll_name + ' prediction'
        df_forecasts.columns = [text]
        print(df_forecasts.tail(20))
    def train_test_forecast_history_arima(self, order=(1,1,1)):
        bla = self._arima.forecasted_from_history(self._train, self._test, order)
        bla = bla[len(self._train):]
        dr_time = pd.date_range(start=self._test.index[0], periods=len(self._test), freq='D')
        df_forecasts = pd.DataFrame(bla, index=dr_time)
        #self._bu_plot.train_test_forecast(self._train, self._test, df_forecasts)
    def my_data_receiver_arima(self,
        data_range=5000,
        future_range=0,
        p=0,
        d=0,
        q=0,
        P=0,
        D=0,
        Q=0,
        trend='ct'):
        self._output.clear_output()
        self._train = self._data[:data_range].iloc[:,0]
        self._test = self._data[data_range + 1:].iloc[:,0]
        order=(p,d,q)
        self.train_test_forecast_arima(order, future_range)
    def my_data_receiver_sarimax(self,
        data_range=5000,
        future_range=0,
        p=0,
        d=0,
        q=0,
        P=0,
        D=0,
        Q=0,
        trend='ct'):
        self._output.clear_output()
        self._train = self._data[:data_range].iloc[:,0]
        self._test = self._data[data_range + 1:].iloc[:,0]
        order=[(p,d,q),(P,D,Q,12),trend]
        self.train_test_forecast_sarimax(order, future_range)
    def draw_buttons(self, on_change_event_class_name):
        on_change_event = self.my_data_receiver_arima
        if on_change_event_class_name == 'sarima':
            on_change_event = self.my_data_receiver_sarimax
        
        lay = {'width': '500px'}
        p_slider = widgets.IntSlider(layout=lay, value=2, min=0, 
                                     max=7, step=1, 
                                     continuous_update=False, 
                                     description='PACF (p)')
        d_slider = widgets.IntSlider(layout=lay, value=0, min=0, 
                                     max=1, step=1, 
                                     continuous_update=False, 
                                     description='diff (d)')
        q_slider = widgets.IntSlider(layout=lay, value=3, min=0, 
                                     max=7, step=1, 
                                     continuous_update=False, 
                                     description='ACF (q)')

        P_slider = widgets.IntSlider(layout=lay, value=1, min=0, 
                                     max=7, step=1, 
                                     continuous_update=False, 
                                     description='PACF (P)')
        D_slider = widgets.IntSlider(layout=lay, value=1, min=0, 
                                     max=1, step=1, 
                                     continuous_update=False, 
                                     description='diff (D)')
        Q_slider = widgets.IntSlider(layout=lay, value=1, min=0, 
                                     max=7, step=1, 
                                     continuous_update=False, 
                                     description='ACF (Q)')
        trend_cobox=widgets.Dropdown(options=[('no trend','n'), ('constant','c'), ('linear','t'), ('const, linear trend','ct')],
    value='n')

        lab_t = widgets.Label('Trend order')
        lab_s = widgets.Label('Seasonal order')

        options = [(date.strftime('  %Y-%m-%d '), date) for date in 
                   self._data.index]
        lmax = len(self._data)
        lcurrent = len(self._train)
        slider = widgets.IntSlider(layout = {'width': '1000px'}, 
                                   value=lcurrent, min=0, 
                                   max=lmax - 1, 
                                   step=1, 
                                   continuous_update=True, 
                                   description='date index')
        future_slider = widgets.IntSlider(layout = {'width': '1000px'}, 
                                   value=0, min=0, 
                                   max=1459, 
                                   step=1, 
                                   continuous_update=True, 
                                   description='future index')
        my_interactiv_widgets = widgets.interactive(
            on_change_event,
            data_range=slider,
            future_range=future_slider,
            p=p_slider,
            d=d_slider,
            q=q_slider,
            P=P_slider,
            D=D_slider,
            Q=Q_slider,
            trend=trend_cobox)
        my_outputs = {'data_range':slider,
            'future_range':future_slider,
            'p':p_slider,
            'd':d_slider,
            'q':q_slider,
            'P':P_slider,
            'D':D_slider,
            'Q':Q_slider,
            'trend':trend_cobox,}
        self._output = widgets.interactive_output(on_change_event ,my_outputs)
        upper_container = VBox(my_interactiv_widgets.children[:2])
        left_box = VBox(my_interactiv_widgets.children[2:5])
        right_box = VBox(my_interactiv_widgets.children[5:9])
        label_left_box = VBox([lab_t, left_box])
        label_right_box = VBox([lab_s, right_box])
        lower_container = HBox([label_left_box])
        
        if on_change_event_class_name == 'sarima':
            lower_container = HBox([label_left_box, label_right_box])

        self._ui_frame = VBox([upper_container, lower_container])

        display(self._ui_frame, self._output)