from IPython.display import display_html
from os import listdir
from os.path import isfile, join

import numpy as np
import time
import re

def search_item(items, wanted_item):
    for position, item in enumerate(items):
        if item.get_po == wanted_item:
            return position

    raise ValueError("%s was not found in the list." % wanted_item)
    
def df_display(*args):
    html_str=''
    for df in args:
        html_str+=df.to_html()
    display_html(html_str.replace('table','table style="display:inline"'),raw=True)
    
def timer(func, *args):
    def wrapper_time(*args):
        start_time = time.time()
        value = func(*args)
        print('++ Function {}() took {}s'.format(func.__name__,round(time.time() - start_time,2)))
        return value
    return wrapper_time

def experiment_folder(main_folder):
    pass
    #folder = './{}/'.format(main_folder)
    #dirs = [f for f in listdir(folder) if isdir(join(folder, f))]

    #res = [re.findall(r'\d+', dd) for dd in dirs if re.findall(r'\d+', dd)]
    #res = np.array(res).flatten()
    #return '{}/exp{}'.format(folder, 1 + np.amax([int(i) for i in res]))
